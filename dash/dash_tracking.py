import dash
import os
import pathlib
import plotly.express as px
import pandas as pd
import numpy as np
import datetime
import time
import glob
import re

import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import State, Input, Output
from skyfield.api import load, wgs84

APP_DATA = '/mnt/data/spectra/hb/'
app = dash.Dash(__name__)

colors = {
    'background': '#FFFFFF',
    'text_title': '#7FDBFF'
}

#def display_time():
#    return html.H3(id="utc-component", 
#        children=f'The time is: {datetime.datetime.now()}')

satellite_dropdown = dcc.Dropdown(
    id="satellite-dropdown-component",
        options=[
            {'label': 'm2-A', 'value': 'm2a'},
            {'label': 'cuava-1', 'value': 'cuv'},
            {'label': 'MEX', 'value': 'mex'}
        ],
    value='MTL',
    style={'padding': 5, 'flex': 1}
)

antenna_dropdown = dcc.Dropdown(
    id="antenna-dropdown-component",
        options=[
            {'label': 'Hobart-12', 'value': 'Hb'},
            {'label': 'Katherine', 'value': 'Ke'},
            {'label': 'Yarragadee', 'value': 'Yg'},
            {'label': 'Hobart-26', 'value': 'Ho'},
            {'label': 'Ceduna', 'value': 'Cd'}
        ],
    value='MTL',
    style={'padding': 5, 'flex': 5}
)

markdown_text = f'''
### Satellite tracking

Dashboard for satellite tracking - this is just to test how long can the lines be in markdown and in all te system, so that we have an idea of the total length of the project. because bla bla bla,
because we can think that this goes on and on. but iatdfdsjfl; dfdsjfkdjf dsf df dskjfsdjfds fa fd jfdsjkf sf dsjfhsadkl fk fsda fdjshf 
fdhfkjasdhfjk a fhdsjf asdf dsaf dsf asdf ;f dskfads
'''

APP_PATH = str(pathlib.Path(__file__).parent.resolve())[:-5]

## Figure 1
# Generate the spectra
def generate_spectra(antenna='hb', freqs=[0, 32e6], n_spec=0):
    file_name: str = glob.glob(f'{APP_DATA}*.bin')[0]
    try:
        fd = open(file_name, 'rb')
    except:
        print(f'Error file not found')

    fsize: int = os.path.getsize(file_name) / 4

    match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdefgh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_([^_]*)", file_name, re.I)

    FFTpt = int(match.group(6))
    Nfft: int = int(FFTpt / 2 + 1)
    bandwidth: float = 32e6

    fmin: float = freqs[0]
    fmax: float = freqs[1]

    df: int = int(bandwidth / Nfft)
    ff: float = np.arange(Nfft) * df
    bmin: int = int(fmin / df)
    bmax: int = int(fmax / df)

    Nspec: int = int(fsize / Nfft - 1)

    one_spec: float = np.fromfile(file=fd, dtype="float32", count=Nfft, offset=int(n_spec) * Nfft)#offset=Nfft * 4 * Nspec
    one_spec /= one_spec.mean()
    
    zoom_spec = one_spec[bmin : bmax]
    zoom_ff = ff[bmin : bmax]

    #fig = px.line(x=ff, y=10*np.log10(one_spec), color_discrete_sequence=px.colors.qualitative.Vivid)
    fig = px.line(x=zoom_ff, y=10*np.log10(zoom_spec), color_discrete_sequence=px.colors.qualitative.Vivid)
    fig.update_layout(
        title_text=f"<b>Dashboard with the full spectra - {int(bandwidth / 1e6)} MHz</b>",
        width=1500,
        height=700,
        font_family="lato",
        font_size=19,
    ).update_xaxes(title='<b>Full spectra in the frequency band [Hz]</b>'
    ).update_yaxes(title_text="<b>Relative Power (dB)</b>")
    fd.close()

    return fig

fig = generate_spectra(antenna='hb', freqs=[0, 32e6], n_spec=0)

## Figure 2 
# Make predictions from the TLE file
frequency_hz = 2221.5e6
def generate_preds(frequency_hz, antenna, satellite):
    ts = load.timescale()
    data_path = os.path.join(APP_PATH,"data")
    satellite = load.tle_file(url=f'{data_path}/{satellite.lower()}.tle', filename=f"{satellite.lower()}.tle")[0]
    
    if 'antenna' == 'hb':
        station = wgs84.latlon(latitude_degrees=-42.8055808638581, longitude_degrees=147.43813722942386, elevation_m=40.983)
    elif 'antenna' == 'ke':
        station = wgs84.latlon(latitude_degrees=-42.8055808638581, longitude_degrees=147.43813722942386, elevation_m=40.983)
    elif 'antenna' == 'yg':
        station = wgs84.latlon(latitude_degrees=-42.8055808638581, longitude_degrees=147.43813722942386, elevation_m=40.983)

    hh = 8
    mm = 20
    ss = range(0, 10 * 60)
    t = ts.utc(2021, 11, 15, hh, mm, ss)

    start_time = hh * 3600 + mm * 60

    tstamp = np.array(ss)
    tstamp += start_time

    pos_sat = (satellite - station).at(t)
    _, _, range_sat, _, _, range_rate_sat = pos_sat.frame_latlon_and_rates(station)

    predicted_doppler = - range_rate_sat.km_per_s * 1e3 / 299792458. * frequency_hz

    fig = px.scatter(x=tstamp, y=predicted_doppler, color_discrete_sequence=px.colors.qualitative.Vivid)
    fig.update_layout(
        title_text=f"<b>Predicted Doppler shift ({frequency_hz / 1e6:.1f} MHz) at {antenna}</b>",
        width=800,
        height=400,
    ).update_xaxes(title='<b>Seconds [s]</b> from start of the scan'
    ).update_yaxes(title_text="<b>Predicted Doppler [Hz]</b>")
    return fig

## Figure 3
# Read data from Fdets file
col_format = ['MJD', 'UTC', 'SNR', 'SMX', 'Fdets', 'Dnoise']
fdets_file: str = glob.glob(f'{APP_DATA}Fdets*.txt')[0]
df_m2a = pd.read_csv(fdets_file, skiprows=4, delimiter=' ', names=col_format)
antenna = html.Div(id="antenna_name")
fig3 = px.scatter(df_m2a, x="UTC", y="Fdets")
fig3.update_layout(
    title_text=f"<b>Doppler shift (centered at {frequency_hz / 1e6:.1f} MHz) detected at {antenna}</b>",
    width=800,
    height=400,
).update_xaxes(title='<b>Seconds [s]</b> from start of the scan'
).update_yaxes(title_text="<b>Topocentric freq dets [Hz]</b>")

# Figures

Spectra = dcc.Graph(id='spectra', 
    figure=fig3,
    #style = {'width' : '100%', 'display' : 'inline-block'}
)

Doppler_predictions = dcc.Graph(id='predictions', 
    figure=generate_preds(frequency_hz, 'Hobart', 'm2a'),
    #style = {'display' : 'block'}
)

Doppler_detections = dcc.Graph(id='detections',
    figure=fig3,
    #style = {'display' : 'block'}
)

app.layout = html.Div(style={'backgroundColor': colors['background']}, 
    children=[
        html.H1(id='title-panel', 
            children='Satellite Tracking Dashboard',
            style={'textAlign': 'center', 'color': colors['text_title']}),
    
        html.Div(dcc.Markdown(id="description-panel", children=markdown_text)),

        html.H3(children='Select satellite and antenna in operations'),

        html.Div(id="panel-upper-lower", 
            children=[
                dcc.Interval(id='interval', interval=10*1000,n_intervals=0),
                html.Div(id='satellite-dropdown', children=satellite_dropdown, style={'width' : '50%', 'display' : 'inline-block'}),
                html.Div(id='antenna-dropdown', children=antenna_dropdown, style={'width' : '50%', 'display' : 'inline-block'}),
            ]
        ),

        #display_time(),

        #html.Div(id="panel-who-knows", 
        #    children=[antenna_dropdown]
        #),

        html.H1(id="satellite-name", children=""),
        html.H1(id="antenna-name", children=""),

        #Doppler_predictions,
        html.Div(id='graphs',
            children=[Spectra,
                html.Div(id = 'right-plots', 
                    children = [Doppler_predictions, Doppler_detections], style={'display' : 'block'})
            ],style={'width' : '50%', 'display' : 'inline-block'}
        ) 
])

'''
Callbacks are the functions that update the staus of the dashboards when something
happens: based on time, a dropdown selection or a click
'''

# Callbacks Dropdown
@app.callback(
    Output("satellite-name", "children"), 
    [Input("satellite-dropdown-component", "value")],
)
def update_satellite_name(val):
    if val == "m2a":
        return "Satellite\nM2-A"
    elif val == "cuv":
        return "Satellite\nCUAVA-1"
    else:
        return ""

@app.callback(
    Output("antenna-name", "children"), 
    [Input("antenna-dropdown-component", "value")],
)
def update_antenna_name(val):
    if val == "Hb":
        return "Hobart-12"
    elif val == "Ke":
        return "Katherine"
    elif val == 'Yg':
        return 'Yarragadee'
    elif val == 'Ho':
        return 'Hobart-26'
    elif val == 'Cd':
        return 'Ceduna'
    else:
        return ""

'''
# Callback to update constantly the time displayed
@app.callback(
    Output("utc-component", "value"),
    [Input("interval", "n_intervals")]
)
def update_time(interval):
    hour = time.localtime(time.time())[3]
    hour = str(hour).zfill(2)

    minute = time.localtime(time.time())[4]
    minute = str(minute).zfill(2)
    return time.time()
'''

@app.callback(
    Output("predictions", "figure"),
    [Input("satellite-dropdown-component", "value"),
    Input("antenna-dropdown-component", "value"),
    Input("interval", "n_intervals")]
)
def update_predictions(antenna, satellite, interval):
    frequency_hz = 2222
    fig = generate_preds(frequency_hz, antenna, satellite)

@app.callback(
    Output("Spectra", "figure"),
    [Input("satellite-dropdown-component", "value"),
    Input("antenna-dropdown-component", "value"),
    Input("interval", "n_intervals")]
)
def update_spectrum(antenna, satellite, interval):
    fig = generate_spectra(antenna, freqs=[0, 32e6], n_spec=0)

if __name__ == '__main__':
    app.run_server(debug=True)