#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Waterfall

v0.1 - First version - 
@author: gofrito
"""
import glob
import os
import pathlib
import time
import re

import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import plotly.express as px
import plotly.graph_objects as go

import dash
from dash.dependencies import Input, Output, State

APP_DATA = '/mnt/data/spectra/hb/'
app = dash.Dash(__name__, routes_pathname_prefix='/dash/')

colors = {
    'background': '#FFFFFF',
    'text_title': '#7FDBFF'
}

# Generate the spectral plot with full bandwidth
def generate_plot(freq_center, antenna):
    APP_DATA = f'/mnt/data/spectra/{antenna}/'
    file_name: str = glob.glob(f'{APP_DATA}*.bin')[0]
    print(file_name)
    try:
        fd = open(file_name, 'rb')
    except:
        print(f'Error file not found')

    fsize: int = os.path.getsize(file_name)

    match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdefgh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_([^_]*)", file_name, re.I)

    satellite: str = match.group(3)
    FFTpt = int(match.group(6))
    bandwidth: float = 32e6
    fc: float = float(freq_center)
    fmin: float = fc - 1e6 # 0.15e6
    fmax: float = fc + 1e6 # 0.15e6
    Nspec: int = int(np.floor(0.5 * fsize / (FFTpt + 1)))
    Nfft: int = int(FFTpt / 2 + 1)
    df: int = int(2 * bandwidth / FFTpt)
    jf: int = np.arange(Nfft)
    ff: float = jf * df

    bmin: int = int(fmin / df)
    bmax: int = int(fmax / df)
    zoom_ff = ff[bmin : bmax]
    zoom_tt = np.arange(Nspec)

    Spectra = np.zeros((Nspec, bmax - bmin))

    for ip in np.arange(Nspec):
        one_spec: float = np.fromfile(file=fd, dtype="float32", count=Nfft, offset=0)
        one_spec /= one_spec.mean()

        zoom_spec = one_spec[bmin : bmax]
        Spectra[ip,:] = 10 * np.log10(zoom_spec)
    
        fig = go.Figure(data = go.Heatmap(y=zoom_tt, x=zoom_ff, z=Spectra, colorscale='Electric'))

        fig.update_layout(
            width=1000,
            height=800,            
            title = f'Tracking of the carrier signal of {satellite} satellite <br>tracked with the {antenna} 12-metre antenna',
            xaxis = dict(title_text = 'Zoom frequency band [Hz]',
                tickfont = dict(family='lato', size=20)),
            yaxis = dict(title_text = 'Time [s]',
                tickfont = dict(family='lato', size=20)),
        )  
    return fig
    fd.close()

    
fig = generate_plot(freq_center=15e6, antenna='hb')

app.layout = html.Div(style={'backgroundColor': colors['background']},
    children = [
        html.H1(id='title-panel', 
            children='Satellite Tracking Dashboard',
            style={'textAlign': 'center', 'color': colors['text_title']}),
        
        html.Div(children='Select the antenna to monitor'),

        html.Div([dcc.Dropdown(id='antenna-dropdown',
            options=[
                {'label' : 'Hobart-12', 'value': 'hb'},
                {'label' : 'Katherine', 'value' : 'ke'},
                {'label' : 'Yarragadee', 'value' : 'yg'}]
            ),html.Div(id='antenna-container')]),

        html.Div(id="main-page",
            children = [
                dcc.Interval(id='interval', interval=100*1000, n_intervals=0),
                dcc.Graph(id='spectrum', figure=fig),
            ]),
        
        html.Div([dcc.Slider(
            id='my-slider',
            min=0,
            max=32e6,
            step=100,
            tooltip={"placement": "bottom", "always_visible": True},
            value=11.3e6
            ),html.Div(id='slider-output-container')
        ])
    ]
)

@app.callback(
    Output("spectrum", "figure"),
    [Input("interval", "n_intervals"),
    Input('my-slider', 'value'),
    Input('antenna-dropdown', 'value')]
)
def update_plot(fig, fc, antenna):
    return generate_plot(fc, antenna)

if __name__ == '__main__':
    app.run_server(debug=True)
