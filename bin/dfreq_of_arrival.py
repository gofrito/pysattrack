#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os
import errno
import pandas as pd

import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument("filename_1", nargs="?", help="Input spectra file")
parser.add_argument("filename_2", nargs="?", help="Input spectra file")
parser.add_argument("-rf", "--rest_frequency", help="Rest frequency in MHz", default=1)

args = parser.parse_args()

if os.path.exists(args.filename_1) & os.path.exists(args.filename_2):
    # Both files exist
    path_1, filename_1 = os.path.split(args.filename_1)
    path_2, filename_2 = os.path.split(args.filename_2)
    path_1: str = f"{os.path.abspath(path_1)}/"
    path_2: str = f"{os.path.abspath(path_2)}/"
else:
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.filename_1)

tags = ["mjd", "time", "Smax", "SNR", "fdets", "rfdets"]
fdets_1 =pd.read_csv(f'{path_1}{filename_1}', skiprows=4, delimiter=' ', names=tags)
fdets_2 =pd.read_csv(f'{path_2}{filename_2}', skiprows=4, delimiter=' ', names=tags)

# read the input file
fd = open(f'{path_1}{filename_1}', 'r')

# discard the first line
fd.readline()

# Second line contains the base frequency
freq_line = fd.readline()
freq_base1: float = float(freq_line.split(' ')[3])

fd.close()

# read the input file
fd = open(f'{path_2}{filename_2}', 'r')

# discard the first line
fd.readline()

# Second line contains the base frequency
freq_line = fd.readline()
freq_base2: float = float(freq_line.split(' ')[3])

fd.close()

if fdets_1['time'][0] > fdets_2['time'][0]:
    print(f"filename_2 starts earlier : {fdets_2['time'][0]}")
    first = fdets_2
    second = fdets_1
    second_base = freq_base2
    first_base = freq_base1
else:
    print(f"filename_1 starts earlier : {fdets_1['time'][0]}")
    first = fdets_1
    second = fdets_2
    second_base = freq_base1
    first_base = freq_base2

ap = first.loc[first['time'] == second['time'][0]].index.values
print(second_base)
print(first_base)
total_length = fdets_2.shape[0]
#print(second['fdets'][:int(total_length - ap)].to_string(index=False))
difference = second['fdets'][ : int(total_length - ap)].values + first_base * 1e6 - first['fdets'][int(ap) : total_length].values - second_base * 1e6
print(difference[0])

c = 299792458 # [m/s]
freq = float(args.rest_frequency) * 1e6

# Calculate the change of velocity per station
v_diff = difference * c / freq

if True:
    plt.plot(second['time'][ : int(total_length - ap)].values, difference)
    plt.xlabel('Time [s]')
    plt.ylabel('Radial velocity [m/s]')
    plt.show()